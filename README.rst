Authentication Service Example
==============================

Service deploy instructions
---------------------------

To install and run service from a docker registry, execute::

  docker run --rm -p 8000:8000 -e DEBUG=1 \
                               -e ALLOWED_HOSTS=127.0.0.1 \
                               -e ALCHEMY_DATABASES=sqlite:///users.db \
                               -e SECRET_KEY=SECRET \
                               registry.gitlab.com/b3b/alchemy:latest


Install and run from a repository
---------------------------------

- Execute: ``git clone https://gitlab.com/b3b/alchemy.git``
- Fill environment file `.env`, for example::

    DEBUG=True
    ALCHEMY_DATABASES=sqlite:///users.db
    ALLOWED_HOSTS=127.0.0.1
    SECRET_KEY=secret

- Execute: ``docker-compose up``


OpenAPI page will be available on `http://127.0.0.1:8000/swagger/`.


Install and run for development
-------------------------------

- Execute: ``git clone https://gitlab.com/b3b/alchemy.git``
- Execute: ``pip install -r requirements-test.txt``
- Run tests: ``pytest``
- Run linter: ``pylint alchemy users swagger``

- Run the application:

  - Set environment variables 
  - Execute: ``./run``
