import datetime

import alembic.config
import pytest
from alchemy import engines
from alchemy.utils import create_session, close_session
from django.test import override_settings
from users.tests.factories import AlchemyModelFactory, CountryFactory, UserFactory


@pytest.fixture
def db():
    with override_settings(ALCHEMY_DATABASES={'default': 'sqlite://'}):
        alembic.config.main(argv=['--raiseerr', 'upgrade', 'head'])
        session = create_session()
        AlchemyModelFactory._meta.sqlalchemy_session = session
        yield session
        close_session()
        engines['default'].dispose()


@pytest.fixture
def user_factory(db):
    return UserFactory


@pytest.fixture
def new_user(new_country, user_factory):
    return user_factory(name='Ivan', middle_name='Ivanovich', last_name='Ivanov', email='ivan@example.org',
                        birth_date=datetime.date(1970, 1, 1))


@pytest.fixture
def country_factory(db):
    return CountryFactory


@pytest.fixture
def new_country(country_factory):
    return country_factory(code='ZW')
