"""Authentication utils."""
from django.conf import settings
import jwt

from users.models import User


def encode_access_token(email):
    """Create infinite access token with a given email payload.

    :param email: User email address
    :returns: access token string
    """
    return jwt.encode({'email': email}, settings.SECRET_KEY, algorithm='HS256').decode()


def decode_access_token(token):
    """Verify and extract payload from a given token.

    :param token: access token
    :returns: payload dict
    """
    try:
        return jwt.decode(token, settings.SECRET_KEY, algorithms=['HS256'])
    except jwt.PyJWTError:
        return None


def authenticate_user(method, credentials):
    """Return user with a given credentials.

    :param method: authentication method
    :param credentials: access token
    :returns: :class:`users.User` instance
    """
    if method.lower() == 'bearer':
        payload = decode_access_token(credentials) or {}
        email = payload.get('email', None)
        if email:
            return User.get_by_email(email)
    return None
