"""Auth API serializers."""
from marshmallow import Schema, fields


class AuthRequestSchema(Schema):
    """Authentication request serializer."""
    email = fields.Email(required=True, allow_none=False, example='ivan@example.org')
    password = fields.String(required=True, allow_none=False, allow_blank=False, example='secret')


class AuthResponseSchema(Schema):
    """Authentication response serializer."""
    user_id = fields.String(example="abcd1")
    access_token = fields.String(example="XXX")


class ProfileSchema(Schema):
    """User profile serializer."""
    user_id = fields.String(attribute='uuid', example="abcd1")
    name = fields.String(example='Ivan')
    last_name = fields.String(example='Ivanov')
    middle_name = fields.String(example='Ivanovich')
    birth_date = fields.String(example='1970-01-01')
    email = fields.String(example='ivan@example.org')
    country = fields.String(attribute='country_name', example='Zimbabwe')
