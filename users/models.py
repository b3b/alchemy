"""User models."""
from django.contrib.auth.hashers import check_password
from sqlalchemy import Column, Date, ForeignKey, Integer, Unicode
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy_utils.types import EmailType, CountryType, UUIDType
from sqlalchemy_utils.models import generic_repr

from alchemy.utils import create_session


UsersBase = declarative_base()


@generic_repr
class Country(UsersBase):
    """Country directory model."""
    __tablename__ = 'users_country'

    id = Column(Integer, primary_key=True)
    code = Column(CountryType, unique=True)

    @property
    def name(self):
        """Name of the country."""
        return self.code and self.code.name


@generic_repr
class User(UsersBase):
    """User model."""
    __tablename__ = 'users_user'

    id = Column(Integer, primary_key=True)
    uuid = Column(UUIDType(binary=False), nullable=False, unique=True)
    name = Column(Unicode(100), nullable=False)
    middle_name = Column(Unicode(100), nullable=False)
    last_name = Column(Unicode(100), nullable=False)
    email = Column(EmailType, nullable=False, unique=True)
    birth_date = Column(Date, nullable=False)
    password_hash = Column(Unicode(128), nullable=False)
    country_id = Column(Integer, ForeignKey('users_country.id'), nullable=False)
    country = relationship('Country', backref='users')

    @property
    def country_name(self):
        """Name of the country."""
        return self.country and self.country.name

    def check_password(self, password):
        """Check if given password matches an user password.

        :param password: password to check
        :returns bool:
        """
        return check_password(password, self.password_hash)

    @classmethod
    def get_by_email(cls, email):
        """Return user with a given email.

        :param email: email of an user
        :returns User:
        """
        return create_session().query(cls).filter_by(email=email).first()
