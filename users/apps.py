"""Authentication example app."""
from django.apps import AppConfig


class UsersConfig(AppConfig):
    """Application config."""
    name = 'users'
