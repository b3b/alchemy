import pytest

from users.utils import encode_access_token


@pytest.fixture
def valid_auth_data():
    return {
        'email': 'ivan@example.org',
        'password': 'secret',
    }


@pytest.fixture
def new_user_token(new_user):
    return encode_access_token(new_user.email)
