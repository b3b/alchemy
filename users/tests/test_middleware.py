import pytest
from users.middleware import CustomUserMiddleware


def test_custom_user_middleware_call_authenticate_user(mocker):
    middleware = CustomUserMiddleware(mocker.Mock())
    request = mocker.Mock()
    authenticate_user = mocker.patch('users.middleware.authenticate_user')
    request.headers = {'authorization': 'Bearer xxx'}

    middleware(request)

    authenticate_user.assert_called_once_with('Bearer', 'xxx')
