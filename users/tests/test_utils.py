from users.utils import authenticate_user, decode_access_token


def test_decode_access_token_payload_returned(new_user, new_user_token):
    assert decode_access_token(new_user_token) == {'email': new_user.email}


def test_authenticate_user_returned(mocker, new_user):
    with mocker.patch('users.utils.decode_access_token', return_value={'email': new_user.email}):
        assert authenticate_user('bearer', 'xxx').id == new_user.id
