import datetime

import pytest
from sqlalchemy.exc import IntegrityError

from users.models import User


def test_country_fields_saved(db, new_country):
    assert new_country.id > 0
    assert new_country.code == 'ZW'
    assert new_country.code.name == 'Zimbabwe'


def test_multiple_countries_created(db, country_factory):
    assert db.query(country_factory._meta.model).count() == 0
    country_factory.create_batch(size=3)
    assert db.query(country_factory._meta.model).count() == 3


def test_country_code_is_unique(db, country_factory):
    country_factory(code='ZW')
    with pytest.raises(IntegrityError):
        country_factory(code='ZW')


def test_user_fields_saved(db, new_user):
    assert new_user.id > 0
    assert new_user.uuid
    assert new_user.email == 'ivan@example.org'
    assert new_user.last_name == 'Ivanov'
    assert new_user.name == 'Ivan'
    assert new_user.middle_name == 'Ivanovich'
    assert isinstance(new_user.birth_date, datetime.date)
    assert new_user.country_id > 0
    assert new_user.password_hash


@pytest.mark.parametrize(
    'password,is_valid', (
        ('secret', True),
        ('secret1', False),
        ('', False)
    )
)
def test_user_check_password(db, new_user, password, is_valid):
    assert new_user.check_password(password) == is_valid


def test_multiple_users_created(db, user_factory, country_factory):
    assert db.query(user_factory._meta.model).count() == 0
    user_factory.create_batch(size=3)
    assert db.query(user_factory._meta.model).count() == 3


def test_user_email_is_unique(db, user_factory):
    user_factory(email='test@example.org')
    with pytest.raises(IntegrityError):
        user_factory(email='test@example.org')


@pytest.mark.parametrize(
    'email,exists', (
        ('ivan@example.org', True),
        ('ivan@example.org1', False),
        ('', False)
    )
)
def test_user_get_by_email(db, new_user, email, exists):
    user = User.get_by_email(email)
    assert bool(user) == exists
    if exists:
        assert user.id == new_user.id
