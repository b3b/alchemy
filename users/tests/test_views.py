from http import HTTPStatus as statuses


def test_auth_get_not_allowed(new_user, client):
    response = client.get('/api/auth')
    assert response.status_code == statuses.METHOD_NOT_ALLOWED


def test_auth_bad_content_type(new_user, client, valid_auth_data):
    response = client.post('/api/auth', data=valid_auth_data)
    assert response.status_code == statuses.BAD_REQUEST
    assert response.json()['errors'] == ['JSON content type expected.']


def test_auth_bad_json(new_user, client, valid_auth_data):
    response = client.post('/api/auth', content_type='application/json', data='test')
    assert response.status_code == statuses.BAD_REQUEST
    assert response.json()['errors'] == ['Invalid JSON format.']


def test_auth_success_status(new_user, client, valid_auth_data):
    response = client.post('/api/auth',
                           content_type='application/json',
                           data=valid_auth_data)
    assert response.status_code == statuses.OK


def test_auth_token_returned(new_user, client, valid_auth_data):
    response = client.post('/api/auth',
                           content_type='application/json',
                           data=valid_auth_data)
    assert response.json()['access_token']


def test_auth_user_id_returned(new_user, client, valid_auth_data):
    response = client.post('/api/auth',
                           content_type='application/json',
                           data=valid_auth_data)
    assert response.json()['user_id']

def test_auth_invalid_password(new_user, client, valid_auth_data):
    data = valid_auth_data
    data['password'] += '11'
    response = client.post('/api/auth',
                           content_type='application/json',
                           data=data)
    assert response.status_code == statuses.UNAUTHORIZED


def test_auth_field_errors(new_user, client, valid_auth_data):
    response = client.post('/api/auth',
                           content_type='application/json',
                           data={})
    assert response.status_code == statuses.BAD_REQUEST
    field_errors = response.json()['field_errors']
    assert set(field_errors.keys()) == {'email', 'password'}


def test_profile_returned(new_user, client, new_user_token):
    response = client.get('/api/profiles/me', content_type='application/json',
                          HTTP_AUTHORIZATION=f"Bearer {new_user_token}")
    assert response.status_code == statuses.OK
    assert response.json() == {
        'user_id': str(new_user.uuid),
        'last_name': 'Ivanov',
        'name': 'Ivan',
        'middle_name': 'Ivanovich',
        'birth_date': '1970-01-01',
        'email': 'ivan@example.org',
        'country': 'Zimbabwe',
    }


def test_profile_forbidden(new_user, client):
    response = client.get('/api/profiles/me', content_type='application/json')
    assert response.status_code == statuses.FORBIDDEN
