import pytest
from users.serializers import AuthRequestSchema


@pytest.mark.parametrize(
    'data,is_valid', (
        ({'email': 'test@example.org', 'password': 'secret'}, True),
        ({'email': 'test@example.org'}, False),
        ({'password': 'secret'}, False),
    )
)
def test_auth_validation(data, is_valid):
    errors = AuthRequestSchema().load(data).errors
    assert bool(errors) == (not is_valid)
