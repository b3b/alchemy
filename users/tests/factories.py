from babel import Locale
from django.contrib.auth.hashers import make_password
import factory
import factory.fuzzy

from users.models import Country, User


country_codes = list(filter(lambda code: code.isalpha(), Locale('en', 'US').territories))
default_password = make_password('secret')


def get_or_create(subfactory):
    """Return first instance of a given factory, create if not exist."""
    session = AlchemyModelFactory._meta.sqlalchemy_session
    instance = None
    instance = session.query(subfactory._meta.model).first()
    if not instance:
        instance = subfactory()
    return instance


class AlchemyModelFactory(factory.alchemy.SQLAlchemyModelFactory):

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        """Save an instance of the model, using shared DB session."""
        cls._meta.sqlalchemy_session = AlchemyModelFactory._meta.sqlalchemy_session
        return super()._create(model_class, *args, **kwargs)


class CountryFactory(AlchemyModelFactory):

    class Meta:
        model = Country
        sqlalchemy_session_persistence = 'commit'

    id = factory.Sequence(lambda n: n + 1)
    code = factory.LazyAttribute(lambda obj: country_codes.pop(0))


class UserFactory(AlchemyModelFactory):

    class Meta:
        model = User
        sqlalchemy_session_persistence = 'commit'

    id = factory.Sequence(lambda n: n + 1)
    uuid = factory.Faker('uuid4')
    name = factory.Faker('first_name')
    middle_name = factory.LazyAttribute(lambda obj: factory.Faker('first_name').generate() + 'ich')
    last_name = factory.Faker('last_name')
    email = factory.Faker('email')
    birth_date = factory.Faker('date_between')
    country = factory.LazyAttribute(lambda obj: get_or_create(CountryFactory))
    password_hash = default_password
