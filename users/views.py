"""Auth API views."""
from http import HTTPStatus as statuses

from django.views.decorators.http import require_GET, require_POST
from django.http import JsonResponse

from alchemy.api_parser import use_args
from users.models import User
from users.serializers import AuthRequestSchema, AuthResponseSchema, ProfileSchema
from users.utils import encode_access_token


@require_POST
@use_args(AuthRequestSchema(strict=True))
def auth_view(request, args):  #pylint: disable=unused-argument
    """Authentication endpoint.
    ---

    post:
      description: Authenticate an user
      requestBody:
          description: User credentials
          content:
            application/json:
              schema: AuthRequestSchema
      responses:
        200:
          description: Successful Authentication
          content:
            application/json:
              schema: AuthResponseSchema
        401:
          description: Authentication failed
    """
    user = User.get_by_email(args['email'])
    if not user or not user.check_password(args['password']):
        return JsonResponse({}, status=statuses.UNAUTHORIZED)
    return JsonResponse(AuthResponseSchema().dump({
        'user_id': user.uuid,
        'access_token': encode_access_token(args['email'])
    }).data)


@require_GET
def profile_view(request):  #pylint: disable=unused-argument
    """User profile endpoint.
    ---

    get:
      description: Get an user profile

      security:
        - JWT: []
      responses:
        200:
          description: Profile of an authenticated user

          content:
            application/json:
              schema: ProfileSchema
        403:
          description: Not authorized
    """
    profile = request.custom_user
    if not profile:
        return JsonResponse({}, status=statuses.FORBIDDEN)
    return JsonResponse(ProfileSchema().dump(profile).data)
