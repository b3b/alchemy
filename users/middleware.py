"""Authentication middleware."""
from users.utils import authenticate_user


class CustomUserMiddleware:
    """Middleware to add a custom authenticated user to a request."""

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        request.custom_user = None
        header = request.headers.get('authorization', '')
        parts = header.split()
        if len(parts) == 2:
            user = authenticate_user(*parts)
            if user:
                request.custom_user = user
        return self.get_response(request)
