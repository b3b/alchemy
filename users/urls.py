"""Authenticatio API endpoints."""
from django.urls import path

from users import views


urlpatterns = [
    path('auth', views.auth_view),
    path('profiles/me', views.profile_view),
]
