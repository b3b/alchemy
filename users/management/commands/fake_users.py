"""fake_users management command."""
import random

import factory
from django.core.management.base import BaseCommand

from alchemy.utils import create_session
from users.models import User
from users.tests.factories import AlchemyModelFactory, CountryFactory, UserFactory


class Command(BaseCommand):
    """Create fake users."""
    help = __doc__

    def handle(self, *args, **options):
        session = create_session()
        AlchemyModelFactory._meta.sqlalchemy_session = session  #pylint: disable=protected-access
        if session.query(User).count() > 0:
            self.stdout.write('DB is already filled, do not create fake values')
            return
        self.stdout.write('Fill DB with fake users')
        countries = CountryFactory.create_batch(10)
        UserFactory(email='ivan@example.org')
        UserFactory.create_batch(
            size=100,
            id=factory.Sequence(lambda n: n + 2),
            country=factory.LazyAttribute(lambda obj: random.choice(countries)),
        )
        self.stdout.write('DB is filled with fake users')
