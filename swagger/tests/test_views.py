from http import HTTPStatus as statuses


def test_profile_returned(client):
    response = client.get('/swagger/swagger.json')
    assert response.status_code == statuses.OK
    assert 'openapi' in response.json()
