"""OpenAPI URLs."""
from django.urls import path

from swagger import views


urlpatterns = [
    path('', views.ui_view),
    path('swagger.json', views.swagger_json, name='swagger_json'),
]
