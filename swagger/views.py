"""OpenAPI views."""
from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin
from django.shortcuts import render
from django.http import JsonResponse

from alchemy.utils import create_session
from swagger.apispec_plugin import DjangoPlugin
from users.views import auth_view, profile_view
from users.models import User


def ui_view(request):
    """Render Swagger UI page.
    Based on https://github.com/swagger-api/swagger-ui
    """
    return render(request, 'index.html', context={
        'credentials': [(user.email, 'secret') for user in create_session().query(User).limit(3)]
    })


def swagger_json(request):  #pylint: disable=unused-argument
    """View that generates OpenAPI specification."""
    spec = APISpec(
        title='Auth example',
        version='1.0.0',
        openapi_version='3.0.2',
        plugins=[DjangoPlugin(), MarshmallowPlugin()],
    )
    spec.components.security_scheme('JWT', {'type': 'http', 'scheme': 'bearer', 'bearerFormat': 'JWT'})
    for view in [auth_view, profile_view]:
        spec.path(django_view=view)
    return JsonResponse(spec.to_dict())
