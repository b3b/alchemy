"""Plugins for apispec."""
from apispec import BasePlugin
from apispec.yaml_utils import load_operations_from_docstring
from django.urls import reverse


class DjangoPlugin(BasePlugin):
    """Django plugin for apispec."""

    def path_helper(self, **kwargs):  #pylint: disable=arguments-differ
        """Path helper for django view."""
        try:
            view = kwargs['django_view']
        except KeyError:
            return None
        return reverse(view)

    def operation_helper(self, operations, **kwargs):  #pylint: disable=arguments-differ
        """Helper to parse operations specification from a django view docstring."""
        try:
            view = kwargs['django_view']
        except KeyError:
            pass
        else:
            operations.update(load_operations_from_docstring(view.__doc__))
