import os
import sys
from logging.config import fileConfig

from sqlalchemy import create_engine
from sqlalchemy import pool

from alembic import context

os.environ['DJANGO_SETTINGS_MODULE'] = 'alchemy.settings'
sys.path.insert(0, os.getcwd())

from alchemy import engines
from users.models import UsersBase

target_metadata = [UsersBase.metadata]


def run_migrations_online():
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    with engines['default'].connect() as connection:
        context.configure(
            connection=connection, target_metadata=target_metadata
        )

        with context.begin_transaction():
            context.run_migrations()


run_migrations_online()
