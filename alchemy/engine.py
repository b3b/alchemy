"""SQLAlchemy engine utils."""
from threading import local

from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.utils.functional import cached_property
from sqlalchemy import create_engine


DEFAULT_DB_ALIAS = 'default'


class EngineHandler:
    """SQLAlchemy engines dictionary, mimic `django.db.connections`.

    >>> from alchemy import engines
    >>> engines['default']
    Engine(sqlite:///users.db)
    """

    def __init__(self):
        self._databases = None
        self._engines = local()

    @cached_property
    def databases(self):
        """Databases configuration from the settings."""
        if self._databases is None:
            self._databases = settings.ALCHEMY_DATABASES

        if DEFAULT_DB_ALIAS not in self._databases:
            raise ImproperlyConfigured(f"You must define a '{DEFAULT_DB_ALIAS}' database connection string.")
        return self._databases

    def __getitem__(self, alias):
        if hasattr(self._engines, alias):
            return getattr(self._engines, alias)

        engine = create_engine(self.databases[alias])
        setattr(self._engines, alias, engine)
        return engine
