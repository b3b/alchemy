"""Alchemy project utils."""
from sqlalchemy.orm import sessionmaker

from alchemy import engines


def create_session(engine_alias='default'):
    """Session factory.

    :param engine_alias: Name of an engine to bind
    :returns: New session, binded to an engine
    """
    engine = engines[engine_alias]

    # reuse session
    session = getattr(engine, '_current_session', None)
    if session:
        return session

    # reuse sessionmaker
    maker = getattr(engine, '_sessionmaker', None)
    if not maker:
        maker = sessionmaker(bind=engines[engine_alias])
        setattr(engine, '_sessionmaker', maker)

    session = maker()
    setattr(engine, '_current_session', session)
    return session


def close_session(engine_alias='default'):
    """Close default session.

    :param engine_alias: Name of an session engine
    """
    engine = engines[engine_alias]
    session = getattr(engine, '_current_session', None)
    if session:
        setattr(engine, '_current_session', None)
        session.close()
