"""webargs API arguments parsers."""
import json

import marshmallow.exceptions
from webargs.core import is_json

from webargs.djangoparser import DjangoParser


class APIParser(DjangoParser):  #pylint: disable=abstract-method
    """API request arguments parser."""

    DEFAULT_LOCATIONS = ['json']

    def _parse_request(self, schema, req, locations):
        """Return a parsed arguments dictionary for the current request.
        Prohibits requests with a non JSON content type.
        """
        if not is_json(req.content_type):
            raise marshmallow.exceptions.ValidationError(['JSON content type expected.'])
        try:
            return super()._parse_request(schema, req, locations)
        except json.JSONDecodeError:
            raise marshmallow.exceptions.ValidationError(['Invalid JSON format.'])



use_args = APIParser().use_args
