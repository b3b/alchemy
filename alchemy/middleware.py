"""Alchemy project middleware."""
import marshmallow.exceptions
from django.http import JsonResponse
from django.utils.deprecation import MiddlewareMixin

from alchemy.utils import close_session


class ValidationErrorResponseMiddleware(MiddlewareMixin):
    """Middleware to produce JSON responses for API errors."""

    def process_exception(self, request, exception):  #pylint: disable=unused-argument
        """Process API exceptions."""
        if isinstance(exception, marshmallow.exceptions.ValidationError):
            if exception.field_names:
                return JsonResponse({'field_errors': exception.messages}, status=400)
            return JsonResponse({'errors': exception.messages}, status=400)
        return None


class AlchemySessionMiddleware:
    """Middleware to close a default session after request is processed."""

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        close_session()
        return response
