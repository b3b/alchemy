"""Project URL configuration."""
from django.urls import include, path


urlpatterns = [
    path('api/', include('users.urls')),
    path('swagger/', include('swagger.urls')),
]
